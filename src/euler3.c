#include <stdio.h>
#include <stdbool.h>

bool millerRabin(unsigned int n) {
    unsigned int s = 50;
    unsigned int i;
    for (i = 1; i < s + 1; i++) {
        //
        int a = random(n - 1) + 1;
        if (test(a, n)) {
            return false;
        }
        return true;
    }
    return false;
}

unsigned int factorPrime(unsigned int n) {
    if (millerRabin(n)) return n;
    
    unsigned int middle = n/2;
    unsigned int i;
    for (i = 2; i < middle; i++) {
        printf("step: %i.\n", i);
        if (n % i == 0) {
            unsigned int f = n / i;
            
            printf("prime: %i.\n", f);
            //check prime
        }
    }
    return 0;
}

int main() {
  printf("out: %i.\n", factorPrime(13195));

  return 0;
}