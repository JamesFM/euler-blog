def sieve(n):
    start = 2
    numbers = range(start, n + 1)
    placeholder = 0
    
    #set composite numbers as a placeholder value
    for i in range(len(numbers)):
        cur = numbers[i]
        if cur != placeholder:
            multiple = cur
            while cur*multiple <= n:
                listPos = (cur*multiple) - start
                numbers[listPos] = placeholder
                multiple += 1
    
    #make a list of primes
    primes = []
    for i in numbers:
        if i != placeholder:
            primes.append(i)
    
    return primes

def sieveSlow(n):
    start = 2
    numbers = range(start, n + 1)
    
    for i in numbers:
        multiple = i
        while i * multiple <= n:
            try:
                numbers.remove(i * multiple)
            except ValueError:
                pass
            multiple += 1
            
    return numbers

if __name__ == '__main__':
    primes = sieve(2000000)
    sum = 0
    for i in primes:
        sum += i
    print sum
