import random

def factorPrimes(n):
    if checkPrime(n):
        return n
    
    for i in xrange(2, n/2):
        #print i
        if (n%i==0):
            f=n/i
            #print f
            if checkPrime(f):
                return f

def checkPrime(n):
    return millerRabin(n)
    
def millerRabin(n, s = 50):
      """
        MillerRabin(n, s = 1000) -> bool Checks whether n is prime or not

        Returns:
          - True, if n is probably prime.
          - False, if n is complex.
      """
      for j in xrange(1, s + 1):
        a = random.randint(1, n - 1)
        if (test(a, n)):
          return False # n is complex
      return True # n is prime

def test(a, n):
    """
    test(a, n) -> bool Tests whether n is complex.

    Returns:
      - True, if n is complex.
      - False, if n is probably prime.
    """
    b = toBinary(n - 1)
    d = 1
    for i in xrange(len(b) - 1, -1, -1):
      x = d
      d = (d * d) % n
      if d == 1 and x != 1 and x != n - 1:
        return True # Complex
      if b[i] == 1:
        d = (d * a) % n
    if d != 1:
      return True # Complex
    return False # Prime

def toBinary(n):
  r = []
  while (n > 0):
    r.append(n % 2)
    n = n / 2
  return r


def main():
    print factorPrimes(600851475143) #13195)

if __name__ == '__main__':
    main()
