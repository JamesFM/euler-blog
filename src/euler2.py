"""
Euler 2
"""

if __name__ == '__main__':
    sum = 0
    cur = 1
    prev = 0

    while cur + prev <= 4000000:
        newPrev = cur
        cur = cur + prev
        prev = newPrev
        if (cur % 2 == 0):
            sum = sum + cur
    print sum