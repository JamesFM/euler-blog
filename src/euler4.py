def palindrome(n):
    nString = str(n)
    #reverse string, start:end:step
    # step -1 moves backwards through string
    if nString == nString[::-1]:
        return True
    return False

if __name__ == '__main__':
    i = 999
    j = 999
    pal = 0
    
    while j > 99:
        result = i * j
        if palindrome(result):
            #print i, ' : ', j, ' : ', result
            if result > pal:
                pal = result
            #break
        i -= 1
        if i < 100:
            i = 999
            j -= 1
    print pal