package euler;

/**
 * Project Euler Problem 6
 */
public class Euler6 {

    private static int sumOfTheSquares(int n) {
        int sum = 0;

        for (int i = 1; i <= n; i++) {
            sum += i * i;
        }

        return sum;
    }

    private static int squareOfTheSum(int n) {
        int sum = 0;

        for (int i = 1; i <= n; i++) {
            sum += i;
        }

        return sum * sum;
    }

    public static void main(String[] args) {
        int n = 100;
        int diff = squareOfTheSum(n) - sumOfTheSquares(n);
        System.out.println(diff);
    }
}